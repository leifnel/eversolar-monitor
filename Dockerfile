FROM debian:bullseye-slim

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y gcc perl cpanminus curl
RUN apt-get install -y make
RUN cpanm AppConfig JSON DBI Net::FTP File::Copy File::Basename Net::MQTT::Simple HTTP::Server::Simple::CGI DBD::SQLite Device::SerialPort
#RUN apt-get install -y  apt-utils wget gnupg && wget https://repo.mosquitto.org/debian/mosquitto-repo.gpg.key
#RUN apt-key add mosquitto-repo.gpg.key && \
#cd /etc/apt/sources.list.d/ && \
#wget http://repo.mosquitto.org/debian/mosquitto-stretch.list && \
#apt-get update -y && \ 
#apt-get install -y gawk mosquitto-clients watch

#RUN mkdir -p  /usr/local/eversolar-monitor/db

COPY eversolar.pl eversolar.ini  README.md /usr/local/eversolar-monitor/
ADD www  /usr/local/eversolar-monitor/www

WORKDIR /usr/local/eversolar-monitor
CMD [ "/usr/local/eversolar-monitor/eversolar.pl" ]





